package org;

import org.spring.AppConfig;
import org.spring.model.Pelicula;
import org.spring.service.PeliculaService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/**
 * Created by dokannon on 21-04-17.
 */
public class Main {

    public static void main (String... args){

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        PeliculaService service = (PeliculaService) context.getBean("peliculaService");
        Pelicula pelicula = new Pelicula();
        pelicula.setIdioma("asdasd");
        pelicula.setNombre("a");
        pelicula.setIdPelicula(1);

        service.save(pelicula);

        context.close();
    }

}
