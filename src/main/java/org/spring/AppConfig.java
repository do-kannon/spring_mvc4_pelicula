package org.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;

/**
 * Created by dokannon on 21-04-17.
 */
@Controller
@ComponentScan(value = "org.spring")
public class AppConfig {
}
