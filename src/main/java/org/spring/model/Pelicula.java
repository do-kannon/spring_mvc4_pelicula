package org.spring.model;

import javax.persistence.*;

/**
 * Created by dokannon on 21-04-17.
 */
@Entity
@Table(name ="PELICULA")
public class Pelicula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="ID_PELICULA")
    private int idPelicula;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="IDIOMA")
    private String idioma;

    public int getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(int idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
