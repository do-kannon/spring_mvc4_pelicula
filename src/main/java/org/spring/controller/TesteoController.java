package org.spring.controller;

import org.spring.AppConfig;
import org.spring.model.Pelicula;
import org.spring.service.PeliculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by dokannon on 23-04-2017.
 */
@Controller
public class TesteoController {

    @Autowired
    PeliculaService peliculaService;

    @RequestMapping(method = RequestMethod.GET)
    public String sayHello(ModelMap model) {
        model.addAttribute("testeo", "Hello World from Spring 4 MVC");
        return "index";
    }

    @RequestMapping(value = "/helloagain", method = RequestMethod.GET)
    public String sayHelloAgain(ModelMap model) {
        model.addAttribute("testeo", "Hello World Again, from Spring 4 MVC");
        return "index";
    }

    @RequestMapping(value="/index", method=RequestMethod.POST)
    public ModelAndView insertaPelicula(@RequestParam("nombre")String name, @RequestParam("idioma")String idioma ){
        ModelAndView model = new ModelAndView("/index");

        Pelicula pelicula = new Pelicula();
        pelicula.setIdioma(idioma);
        pelicula.setNombre(name);
        pelicula.setIdPelicula(1);

        peliculaService.save(pelicula);

        return model;
    }

}


