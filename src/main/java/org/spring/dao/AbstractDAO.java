package org.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.spring.model.Pelicula;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
public abstract class AbstractDAO<T> {

    @Autowired
    private SessionFactory sessionFactory;


    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public void update (T pelicula){
        getSession().update(pelicula);
    }

    public void save (T pelicula){
        getSession().save(pelicula);
    }

    public void delete (T pelicula){
        getSession().delete(pelicula);
    }

    public List<T> findAll (){
       return getSession().createCriteria(Pelicula.class).list();
    }

}
