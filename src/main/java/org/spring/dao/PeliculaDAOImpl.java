package org.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.spring.model.Pelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
@Repository(value = "peliculaDAO")
public class PeliculaDAOImpl extends AbstractDAO implements PeliculaDAO {

    
    @Override
    public void update(Pelicula pelicula)    {
            getSession().update(pelicula);
    }

    @Override
    public void save(Pelicula pelicula) {

        getSession().save(pelicula);
    }

    @Override
    public List<Pelicula> findAllPeliculas() {

        return getSession().createCriteria(Pelicula.class).list();
    }

    @Override
    public List<Pelicula> findByNombre(String nombre) {
        return getSession().createCriteria(Pelicula.class).add(Restrictions.like("NOMBRE",nombre)).list();
    }

    @Override
    public void delete(Pelicula pelicula) {
        getSession().delete(pelicula);
    }
}
