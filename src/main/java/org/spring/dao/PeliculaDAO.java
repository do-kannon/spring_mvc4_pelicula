package org.spring.dao;

import org.spring.model.Pelicula;

import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
public interface PeliculaDAO {

    void update(Pelicula pelicula);

    void save(Pelicula pelicula);

    List<Pelicula> findAllPeliculas();

    List<Pelicula> findByNombre(String nombre);

    void delete (Pelicula pelicula);

}
