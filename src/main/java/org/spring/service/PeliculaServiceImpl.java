package org.spring.service;

import org.spring.dao.PeliculaDAO;
import org.spring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
@Service("peliculaService")
@Transactional
public class PeliculaServiceImpl implements PeliculaService {

    @Autowired
    PeliculaDAO peliculaDAO;

    @Override
    public void update(Pelicula pelicula) {
        peliculaDAO.update(pelicula);
    }

    @Override
    public void save(Pelicula pelicula) {
        peliculaDAO.save(pelicula);
    }

    @Override
    public List<Pelicula> findAllPeliculas(Pelicula pelicula) {
        return peliculaDAO.findAllPeliculas();
    }

    @Override
    public List<Pelicula> findByName(String nombre) {
        return peliculaDAO.findByNombre(nombre);
    }
}
