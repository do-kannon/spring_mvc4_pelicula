package org.spring.service;

import org.spring.model.Pelicula;

import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
public interface PeliculaService {

    void update (Pelicula pelicula);

    void save (Pelicula pelicula );

    List<org.spring.model.Pelicula> findAllPeliculas( Pelicula pelicula);

    List<org.spring.model.Pelicula> findByName (String nombre);
}
